import './App.scss';
import Content from './components/Content';
import Footer from './components/Footer';
import { ResponsiveProvider } from './contexts/ResponsiveContext';

const App = () => (
  <ResponsiveProvider>
    <div className="wrapper">
      <Content />
    </div>
    <Footer />
  </ResponsiveProvider>
);

export default App;
