import ContentBody from './ContentBody';
import ContentHeader from './ContentHeader';

const Content = () => (
  <div className="content">
    <ContentHeader />
    <ContentBody />
  </div>
);

export default Content;
