import { useContext } from 'react';
import { FaGithub, FaGitlab, FaLinkedin } from 'react-icons/fa';
import ResponsiveContext from '../contexts/ResponsiveContext';
import Educations from './Educations';
import Experiences from './Experiences';
import Sidebar from './Sidebar';
import Socials from './Socials';

const ContentBody = () => {
  const { isMobile } = useContext(ResponsiveContext);

  return (
    <div className="body">
      <div className="left">
        <Experiences
          title="Work Experience"
          list={[
            {
              date: {
                start: '07/2022',
                end: '11/2023',
              },
              description:
                'React frontend developer, quickly acted as lead frontend and mentor in JS and React. I work on MERN, Ruby on Rails and Nextjs projects focused on maintenance and scalability.',
              links: [
                {
                  href: 'https://site.uda.ca/',
                  text: 'site.uda.ca',
                },
              ],
              location: 'Chicoutimi, Canada',
              title: 'Front-End developer',
              subtitle: 'La Web Shop',
            },
            {
              date: {
                start: '07/2008',
                end: '07/2019',
              },
              description:
                'Worked as a project manager for IT and construction companies, managing teams of up to 20 people and budgets of up to 6M€.',
              links: [],
              location: 'France and Canada',
              title: 'Project Manager',
              subtitle: 'Construction + IT industries',
            },
          ]}
        />
        <Experiences
          title="Other Experiences"
          list={[
            {
              date: {
                start: '07/2019',
                end: 'Present',
              },
              description:
                'Management of all aspects for a cooking channel, including content creation to building a large audience of 400k+ subscribers along with coding a website to publish text recipes.',
              links: [
                {
                  href: 'https://www.cuisinierrebelle.com',
                  text: 'cuisinierrebelle.com',
                },
              ],
              location: 'Montreal, Canada',
              title: 'Youtube',
              subtitle: 'Content creator',
            },
          ]}
        />
        <Educations
          title="Courses & Certifications"
          list={[
            {
              year: '2022',
              text: 'Concordia university, Full-Stack Web Development (MERN)',
            },
            {
              year: '2019',
              text: 'Le Wagon Bootcamp, Full-Stack Web Development (Ruby on Rails)',
            },
          ]}
        />
        <Educations
          title="Education"
          list={[
            {
              year: '2008',
              text: 'Université de Cergy-Pontoise, Master degree in Civil Engineering',
            },
          ]}
        />
        <Socials
          list={[
            {
              name: 'LinkedIn',
              url: 'https://www.linkedin.com/in/yannicklescure',
              Icon: FaLinkedin,
            },
            {
              name: 'GitHub',
              url: 'https://github.com/yannicklescure',
              Icon: FaGithub,
            },
            {
              name: 'Gitlab',
              url: 'gitlab.com/yannicklescure/resume',
              Icon: FaGitlab,
            },
          ]}
        />
        {isMobile && <Sidebar />}
      </div>
      {!isMobile && <Sidebar />}
    </div>
  );
};

export default ContentBody;
