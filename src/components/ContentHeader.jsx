import { FaEnvelope } from 'react-icons/fa';
import Description from './Description';

const ContentHeader = () => (
  <div className="header">
    <div className="title">
      <h1>Yannick Lescure</h1>
      <a
        href="mailto:yannick.lescure@gmail.com"
        className="item"
        aria-label="contact email"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FaEnvelope /> yannick.lescure@gmail.com
      </a>
    </div>
    <hr />
    <Description />
  </div>
);

export default ContentHeader;
