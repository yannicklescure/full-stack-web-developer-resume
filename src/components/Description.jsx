const Description = () => (
  <div className="description">
    <p>
      <strong>Full-Stack developer</strong> with a keen interest in buildings applications that help
      people. Previously in a lead frontend developer position, I am open for new opportunities to
      put my skills to work on improving ways for people to use software in their lives.
    </p>
    <p>
      My years as Project Manager make me good at organizing and delegating work to motivate people
      in order to produce excellence. Furthermore, as a problem solver, I love to take a hands-on
      approach and jump in to do any part of a job that needs doing!
    </p>
  </div>
);

export default Description;
