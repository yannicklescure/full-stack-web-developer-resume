import { FaFileDownload } from 'react-icons/fa';

const DownloadLinks = () => (
  <div className="files">
    <h4>Downloads</h4>
    <ul>
      <li>
        <FaFileDownload />
        <a
          href="https://yannicklescure.gitlab.io/full-stack-web-developer-resume/resume-yannick-lescure.pdf"
          aria-label="pdf file link"
          target="_blank"
          rel="noreferrer noopener"
        >
          pdf file
        </a>
      </li>
      <li>
        <FaFileDownload />
        <a
          href="https://yannicklescure.gitlab.io/full-stack-web-developer-resume/resume-yannick-lescure.docx"
          aria-label="docx file link"
          target="_blank"
          rel="noreferrer noopener"
        >
          docx file
        </a>
      </li>
    </ul>
  </div>
);

export default DownloadLinks;
