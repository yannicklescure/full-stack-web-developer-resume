const Education = ({ text, year }) => (
  <div className="education">
    <p className="education-year">{year}</p>
    <p className="education-text">{text}</p>
  </div>
);

export default Education;
