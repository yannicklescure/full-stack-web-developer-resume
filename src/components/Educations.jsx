import { v4 as uuidv4 } from 'uuid';
import Education from './Education';

const Educations = ({ list, title }) => (
  <div className="educations">
    <h2>{title}</h2>
    <ul>
      {list.map((props) => (
        <li key={uuidv4()}>
          <Education {...props} />
        </li>
      ))}
    </ul>
  </div>
);

export default Educations;
