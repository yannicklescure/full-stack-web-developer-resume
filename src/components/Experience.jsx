import { v4 as uuidv4 } from 'uuid';

const Experience = ({ date, description, links, location, title, subtitle }) => (
  <div className="experience">
    <div className="experience-header">
      <div className="experience-title">
        <h4>{title}</h4>
        <h6>
          {subtitle} - {location}
        </h6>
      </div>

      <div className="experience-date">
        <h6>
          {date.start} - {date.end}
        </h6>
      </div>
    </div>

    <div className="experience-body">
      <p className="experience-description">{description}</p>
      <ul>
        {links.map(({ href, text }) => (
          <li key={uuidv4()}>
            <a href={href} target="_blank" rel="noopener noreferrer">
              {text}
            </a>
          </li>
        ))}
      </ul>
    </div>
  </div>
);

export default Experience;
