import { v4 as uuidv4 } from 'uuid';
import Experience from './Experience';

const Experiences = ({ list, title }) => (
  <div className="experiences">
    <h2>{title}</h2>
    {list.map((props) => (
      <Experience key={uuidv4()} {...props} />
    ))}
  </div>
);

export default Experiences;
