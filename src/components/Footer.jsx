import MadeWithReact from './MadeWithReact';

export default function Footer() {
  return (
    <footer className="footer">
      <div className="footer-content">
        <MadeWithReact />
      </div>
    </footer>
  );
}
