import { FaLink, FaReact } from 'react-icons/fa';

export default function MadeWithReact() {
  return (
    <a
      href="https://gitlab.com/yannicklescure/full-stack-web-developer-resume"
      target="_blank"
      rel="noopener noreferrer"
      className="made-with-react"
    >
      <p className="footer-content-text">Made with React</p>
      <FaReact />
      <p>by Yannick Lescure</p>
      <FaLink className="external-link" />
    </a>
  );
}
