import { useEffect } from 'react';

const Picture = ({ alt = '', src, isSquare = false, width, height }) => {
  const [dimensions, setDimensions] = React.useState({ width: 0, height: 0 });

  useEffect(() => {
    setDimensions({
      width,
      height: isSquare ? width : height,
    });
  }, []);

  return <img src={src} alt={alt} className="profile-picture" {...dimensions} />;
};

export default Picture;
