import React from 'react';
import { FaLeaf } from 'react-icons/fa';

const PrintButton = () => (
  <div className="print-button">
    <button type="button" onClick={() => window.print()}>
      <FaLeaf className="icon" />
      <span>Save as pdf</span>
    </button>
  </div>
);

export default PrintButton;
