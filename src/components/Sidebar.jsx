import { useContext } from 'react';
import ResponsiveContext from '../contexts/ResponsiveContext';
import DownloadLinks from './DownloadLinks';
// import ProfilePicture from '../assets/images/profile-picture.jpg';
// import Picture from './Picture';
import Skills from './Skills';

const Sidebar = () => {
  const { isMobile } = useContext(ResponsiveContext);

  return (
    <div className="sidebar">
      {!isMobile && <DownloadLinks />}
      <Skills
        title="Tech stack"
        list={[
          {
            title: 'Full-Stack',
            list: [
              {
                text: 'MERN',
                url: 'https://www.mongodb.com/mern-stack',
              },
              {
                text: 'Nextjs',
                url: 'https://nextjs.org/',
              },
              {
                text: 'Nuxt',
                url: 'https://nuxt.com/',
              },
              {
                text: 'Ruby on Rails',
                url: 'https://rubyonrails.org/',
              },
            ],
          },
          {
            title: 'Front-End',
            list: [
              {
                text: 'React',
                url: 'https://react.dev/',
              },
              {
                text: 'Vue',
                url: 'https://vuejs.org/',
              },
              {
                text: 'jQuery',
                url: 'https://jquery.com/',
              },
              {
                text: 'Bootstrap',
                url: 'https://getbootstrap.com/docs/5.0/getting-started/introduction/',
              },
              {
                text: 'Sass',
                url: 'https://sass-lang.com/',
              },
            ],
          },
          {
            title: 'Back-End',
            list: [
              {
                text: 'Node JS',
                url: 'https://nodejs.org/en',
              },
              {
                text: 'Express',
                url: 'https://expressjs.com/',
              },
              {
                text: 'PostgreSQL',
                url: 'https://www.postgresql.org/',
              },
              {
                text: 'MongoDB',
                url: 'https://www.mongodb.com/',
              },
              {
                text: 'MySQL',
                url: 'https://www.mysql.com/',
              },
              {
                text: 'Redis',
                url: 'https://redis.io/',
              },
              {
                text: 'ElasticSearch',
                url: 'https://www.elastic.co/',
              },
            ],
          },
          {
            title: 'Languages',
            list: [
              {
                text: 'JavaScript',
                url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript',
              },
              {
                text: 'Ruby',
                url: 'https://www.ruby-lang.org/en/',
              },
              {
                text: 'Python',
                url: 'https://www.python.org/',
              },
              {
                text: 'SQL',
                url: 'https://en.wikipedia.org/wiki/SQL',
              },
              {
                text: 'HTML',
                url: 'https://whatwg.org/',
              },
              {
                text: 'CSS',
                url: 'https://www.w3.org/TR/CSS/',
              },
            ],
          },
          {
            title: 'Other',
            list: [
              {
                text: 'Git',
                url: 'https://git-scm.com/',
              },
              {
                text: 'Docker',
                url: 'https://www.docker.com/',
              },
              {
                text: 'Github Copilot',
                url: 'https://github.com/features/copilot',
              },
              {
                text: 'Heroku',
                url: 'https://www.heroku.com/',
              },
              {
                text: 'AWS',
                url: 'https://aws.amazon.com/',
              },
            ],
          },
        ]}
      />
    </div>
  );
};

export default Sidebar;
