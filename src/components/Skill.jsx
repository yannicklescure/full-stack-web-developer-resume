import { v4 as uuidv4 } from 'uuid';
import { sortArrayByKey } from '../helpers';

const Skill = ({ list, title }) => (
  <div className="section">
    <h4>{title}</h4>
    <ul>
      {sortArrayByKey(list, 'text').map(({ text, url }) => (
        <li key={uuidv4()} className="item">
          <a href={url} target="_blank" rel="noopener noreferrer">
            {text}
          </a>
        </li>
      ))}
    </ul>
  </div>
);

export default Skill;
