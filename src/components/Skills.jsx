import { v4 as uuidv4 } from 'uuid';
import Skill from './Skill';

const Skills = ({ list, title }) => (
  <div className="sections">
    <h3>{title}</h3>
    <ul>
      {list.map((props) => (
        <li key={uuidv4()}>
          <Skill {...props} />
        </li>
      ))}
    </ul>
  </div>
);

export default Skills;
