const cleanUrl = (url) => url.replace(/(mailto:)?(https?:\/\/)?(www\.)?/, '');

const Social = ({ name, url, Icon }) => (
  <li key={name}>
    <a href={url} target="_blank" rel="noopener noreferrer">
      <div className="item">
        <Icon size={16} />
        <p>{cleanUrl(url)}</p>
      </div>
    </a>
  </li>
);

export default Social;
