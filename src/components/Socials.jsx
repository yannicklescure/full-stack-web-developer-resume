import { v4 as uuidv4 } from 'uuid';
import Social from './Social';

const Socials = ({ list }) => (
  <div className="socials">
    <ul>
      {list.map((props) => (
        <Social key={uuidv4()} {...props} />
      ))}
    </ul>
  </div>
);

export default Socials;
