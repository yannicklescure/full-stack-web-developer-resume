import { createContext, useEffect, useMemo, useState } from 'react';
import useWindowSize from '../hooks/useWindowSize';

const ResponsiveContext = createContext();

export const ResponsiveProvider = ({ children }) => {
  const [isMobile, setIsMobile] = useState(false);

  const windowSize = useWindowSize();

  useEffect(() => {
    setIsMobile(window.innerWidth < 768);
  }, [windowSize]);

  const memoizedValue = useMemo(() => ({ isMobile }), [isMobile]);

  return <ResponsiveContext.Provider value={memoizedValue}>{children}</ResponsiveContext.Provider>;
};

export default ResponsiveContext;
