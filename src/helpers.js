async function sleep(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}

const shuffleArray = (array) => {
  const arr = [...array];
  let i = array.length - 1;
  while (i > 0) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    arr[i] = arr[j];
    arr[j] = temp;
    i -= 1;
  }
  return array;
};

const isValidArray = (arr) => Array.isArray(arr) && arr.length > 0;

const isEmptyArray = (arr) => Array.isArray(arr) && arr.length === 0;

// https://stackoverflow.com/questions/25991367/difference-between-throttling-and-debouncing-a-function
// https://blog.openreplay.com/forever-functional-debouncing-and-throttling-for-performance

const debounce = (fn, timeDelay = 1000) => {
  let fnTimer;
  return (...args) => {
    clearTimeout(fnTimer);
    fnTimer = setTimeout(() => fn(...args), timeDelay);
  };
};

const throttle = (fn, timeDelay = 1000) => {
  let fnTimer;
  return (...args) => {
    if (!fnTimer) {
      fn(...args);
      fnTimer = setTimeout(() => {
        fnTimer = null;
      }, timeDelay);
    }
  };
};

/* How to examples
  - JS -
  const consoleLog = (str) => console.log(str)

  const processThrottle = throttle(consoleLog, 500)
  processThrottle('test')

  const processDebounce = debounce(consoleLog, 500)
  processDebounce('test')

  - React -
  const processDebounce = useCallback(debounce(consoleLog, 500), [])
  processDebounce('test')

  OR with state variable

  const [str, setStr] = useState('')
  setStr('test')
  const processDebounce = useCallback(debounce(consoleLog, 500), [str])
  useEffect(() => {
    processDebounce(str)
  }, [str])
*/

function deleteAllCookies() {
  // eslint-disable-next-line no-shadow
  const cookies = document.cookie.split(';');

  for (let i = 0; i < cookies.length; i += 1) {
    const cookie = cookies[i];
    const eqPos = cookie.indexOf('=');
    const name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
    document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/`;
  }
}

export function isObject(item) {
  return item !== null && typeof item === 'object' && !Array.isArray(item);
}

function objectKeysToCamelCase(obj) {
  if (!obj) return obj;
  const newObj = {};
  Object.entries(obj).forEach(([key, value]) => {
    let newKey = key
      .replace('ium_instances', 'ia')
      .replace('ia_instances', 'ia')
      .replace('y_instances', 'ies')
      .replace('_instances', 's')
      .replace('_instance', '')
      .replace(/_([a-z])/g, (g) => g[1].toUpperCase());
    // prettier-ignore
    if (newKey.endsWith('ss')) newKey = newKey.replace('ss', 's');

    if (Array.isArray(value)) {
      if (value.every((item) => isObject(item))) {
        newObj[newKey] = value.map((item) => objectKeysToCamelCase(item));
      } else if (value.every((item) => Array.isArray(item))) {
        newObj[newKey] = value.map((item) => Object.values(objectKeysToCamelCase(item)));
      } else {
        newObj[newKey] = value;
      }
    } else if (typeof value === 'object' && !Array.isArray(value)) {
      newObj[newKey] = objectKeysToCamelCase(value);
    } else {
      newObj[newKey] = obj[key];
    }
  });
  return newObj;
}

/**
 * This function is used to remove keys from an object
 * @param {*} obj
 * @param {*} keys
 * @returns
 * @example
 * const obj = { a: 1, b: 2, c: 3 };
 * const keys = ['a', 'b'];
 * const newObj = removeKeysFromObject(obj, keys);
 * console.log(newObj); // { c: 3 }
 * console.log(obj); // { a: 1, b: 2, c: 3 }
 */
const removeKeysFromObject = (obj, keys) => {
  const newObj = { ...obj };
  keys.forEach((key) => delete newObj[key]);
  return newObj;
};

/**
 * This function transforms a string from snake_case to camelCase
 * @param {*} str
 * @returns
 * @example
 * const str = 'hello_world';
 * const newStr = snakeToCamel(str);
 * console.log(newStr); // 'helloWorld'
 */
function snakeToCamel(str) {
  return str.replace(/([-_][a-z])/g, (group) =>
    group.toUpperCase().replace('-', '').replace('_', '')
  );
}

const snake2Camel = (str) => snakeToCamel(str);

function cookies() {
  return {
    set: (name, value, days = 7) => {
      let expires = '';
      if (days) {
        const date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = `; expires=${date.toUTCString()}`;
      }
      document.cookie = `${name}=${value || ''}${expires}; path=/`;
    },
    get: (name) => {
      const nameEQ = `${name}=`;
      const ca = document.cookie.split(';');
      let i = 0;
      while (i < ca.length) {
        let c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        i += 1;
      }
      return null;
    },
    getAll: () => {
      const obj = {};
      document.cookie.split(';').forEach((cookie) => {
        const [key, value] = cookie.split('=');
        obj[key.trim()] = value;
      });
      return obj;
    },
    erase: (name) => {
      document.cookie = `${name}=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
    },
  };
}

function deepFreeze(object) {
  // Retrieve the property names defined on object
  const propNames = Reflect.ownKeys(object);

  // Freeze properties before freezing self
  propNames.forEach((name) => {
    const value = object[name];

    if ((value && typeof value === 'object') || typeof value === 'function') {
      deepFreeze(value);
    }
  });

  return Object.freeze(object);
}

function formatDate(date) {
  const [year, month, day] = date.split('-');
  return `${day}/${month}/${year}`;
}

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

function formatToLocaleDateString(date) {
  return new Date(date).toLocaleDateString('fr-FR', options);
}

function sortArrayByKey(array, key, order = 'asc') {
  const sortedArray = [...array];
  return sortedArray.sort((a, b) => {
    const nameA = a[key].toUpperCase(); // ignore upper and lowercase
    const nameB = b[key].toUpperCase(); // ignore upper and lowercase
    let result = 0;
    if (nameA < nameB) {
      result = -1;
    }
    if (nameA > nameB) {
      result = 1;
    }

    // names must be equal
    return order === 'asc' ? result : result * -1;
  });
}

export {
  cookies,
  debounce,
  deepFreeze,
  deleteAllCookies,
  isEmptyArray,
  isValidArray,
  objectKeysToCamelCase,
  removeKeysFromObject,
  shuffleArray,
  sleep,
  snake2Camel,
  snakeToCamel,
  throttle,
  formatDate,
  formatToLocaleDateString,
  sortArrayByKey,
};
